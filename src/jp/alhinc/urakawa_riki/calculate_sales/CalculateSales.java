package jp.alhinc.urakawa_riki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class CalculateSales {
	public static void main(String[] args){
		File dir = new File(args[0]);

		try{
			Map<String, String> inputMap = new HashMap<>();
			Map<String, Long> addSalesMap = new HashMap<>();

			// 支店定義ファイルの読み込み
			File inputBranchFile = new File(dir, "branch.lst");

			if (!inputBranchFile.exists()){
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			BufferedReader br = new BufferedReader(new FileReader(inputBranchFile));
			try{
				String line;
				while((line = br.readLine()) != null){
					List<String> lineDataFormatList = Arrays.asList(line.split(",", 0));

					if(lineDataFormatList.size() != 2){
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					// inputMap(支店コード, 支店名)
					inputMap.put(lineDataFormatList.get(0), lineDataFormatList.get(1));
					addSalesMap.put(lineDataFormatList.get(0), 0l);
				}
			}finally{
				br.close();
			}

			// 売り上げファイルの読み込み
			FilenameFilter filter = new FilenameFilter(){
				public boolean accept(File dir, String str){
					return new File(dir,str).isFile() && str.matches("\\d{8}\\.rcd");
				}
			};

			// .rcdのフィルターに通して配列にする
			File[] salesFiles = dir.listFiles(filter);

			if(salesFiles == null){
				System.out.println("売り上げファイルが存在しません");
				return;
			}

			List<Integer> salesFileNumList = Arrays.stream(salesFiles)
					.map(f -> Integer.parseInt(f.getName().substring(0, 8)))
					.sorted()
					.collect(Collectors.toList());

			int salesFileSize = salesFileNumList.size();
			int salesNumMin = salesFileNumList.get(0);
			int salesNumMax = salesFileNumList.get(salesFileSize -1);

			// 連番チェックのBoolean
			Boolean numRowCheck = ((salesNumMin +  salesFileSize) == salesNumMax + 1);

			if(!numRowCheck){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

			//処理
			// ファイルの中身のフォーマットを確認
			String rcdFileName;
			for(File salesFile : salesFiles){
				BufferedReader salesBufferedReader = new BufferedReader(new FileReader(salesFile));

				try{
					String lineBranch = salesBufferedReader.readLine();
					String lineBranchValue = salesBufferedReader.readLine();
					String lineThurd = salesBufferedReader.readLine();

					if(lineThurd != null){
						System.out.println(salesFile.getName() + "のフォーマットが不正です");
						return;
					}
					if(!addSalesMap.containsKey(lineBranch)){
						System.out.println(salesFile.getName() + "の支店コードが不正です");
						return;
					}

					// その支店コードの合計額(addSalesMapのValue)が10桁を超えた場合、エラー
					Long addValueNum = Long.parseLong(lineBranchValue);
					Long addSalesTotal = addSalesMap.get(lineBranch);
					addSalesTotal += addValueNum;

					// addSalesTotalの絶対値を取る。
					Long addSalesTotalAbs = Math.abs(addSalesTotal);
					String addSalesTotalString = addSalesTotalAbs.toString();

					if(addSalesTotalString.length() > 10){
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					addSalesMap.put(lineBranch, addSalesTotal);
				}finally{
					salesBufferedReader.close();
				}
			}

			// ファイルの出力
			BufferedWriter outputBuffereWriter = new BufferedWriter(new FileWriter(new File(dir, "branch.out")));
			try{
				for(String branchCode : addSalesMap.keySet()){
					outputBuffereWriter.write(branchCode + "," + inputMap.get(branchCode) + "," + addSalesMap.get(branchCode));
					outputBuffereWriter.newLine();
				}
			}finally{
				outputBuffereWriter.close();
			}

		}catch(Exception e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}


